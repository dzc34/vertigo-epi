

4 Mousquetons à Vis  ==> scotch ROUGE
---------------------------------
        
        
        Mousqueton à vis n° 1  
        ---------------------------------
        type :          mousqueton rond à vis
        date d'achat :  24 mai 2014
        achat à :       Expe Montpellier
        Fabricant :     PEZL
        Modèle :        OK Screw Lock (SL)
        Garantie :      3 ans        
            n° de série :   14011VA5203  (YY|DayOfBuild|KeyControl)
            Fabrication :   janvier 2014 (11ème jour de l'année)
            nuréro frappé : 08 13 => ??
        
        
        Mousqueton à vis n° 2 
        ---------------------------------
        type :          mousqueton rond à vis
        date d'achat :  24 mai 2014
        achat à :       Expe Montpellier
        Fabricant :     PEZL
        Modèle :        OK Screw Lock (SL)
        Garantie :      3 ans        
            n° de série :   13264VA0800  (YY|DayOfBuild|KeyControl)
            Fabrication :   Septembre 2013 (264ème jour de l'année)
            nuréro frappé : 03 13 => ??
        
        
        Mousqueton à vis n° 3 
        ---------------------------------
        type :          mousqueton poire automatique
        date d'achat :  ?
        Fabricant :     PEZL
        Modèle :        Vertigo     
            n° de série :   06196VA3915  (YY|DayOfBuild|KeyControl)
            Fabrication :   2006 (basé sur n° série)           

            
        Mousqueton à vis n° 4 
        ---------------------------------
        type :          mousqueton poire automatique
        date d'achat :  ?
        Fabricant :     PEZL
        Modèle :        Vertigo     
            n° de série :   1339-45  + n° : 03 02 
            Fabrication :   ?    (impossible à définir)





    

2 poulies "TANDEM Speed" de PEZL  (Gris bleu)
--------------------------------------------

couleur :       gris (ou bleu-gris)
type :          rapide
          
    Poulie n° 1 
    ---------------
    date d'achat :  22 mai 2014
    achat à :       Expe Montpellier
    Fabricant :     PEZL
    Modèle :        Poulie TANDEM Speed
    n° de série :   14113FC6449  (YY|DayOfBuild|KeyControl)
    Fabrication :   Avril 2014 (113ème jour de l'année)
    Garantie :      3 ans
    
    Poulie n° 2 
    ---------------
    date d'achat :  22 mai 2014
    achat à :       Expe Montpellier
    Fabricant :     PEZL
    Modèle :        Poulie TANDEM Speed
    n° de série :   14113FC6445  (YY|DayOfBuild|KeyControl)
    Fabrication :   Avril 2014 (113ème jour de l'année)
    Garantie :      3 ans

    
    

2 poulies "TANDEM Cable" de PEZL  (Jaune)
--------------------------------------------

couleur :       jaune
type :          lente

    Poulie n° 3
    -----------------
    date d'achat :  ?
    Fabricant :     PEZL
    Modèle :        Poulie TANDEM Cable
    n° de série :   04314F  (YY|DayOfBuild|KeyControl)
    Fabrication :   2004 (basé sur n° série)

    Poulie n°  4 
    -----------------
    date d'achat :  ?
    Fabricant :     PEZL
    Modèle :        Poulie TANDEM Cable
    n° de série :   02177I  (YY|DayOfBuild|IDControler)
    Fabrication :   2002 (basé sur n° série)
            
  

-------------------------------------
La Tandem speed bleue de Petzl est une poulie double à haut rendement
 pouvant être utilisée sur corde comme sur cable. Montée sur roulement
 à bille, elle procure bien plus de vitesse que les modèles Tandem cable
 (la jaune). Elle permettra à des gens possèdant déjà une Tandem cable 
 de se faire d'avantage plaisir sur des tyroliennes ou l'angle de 
 descente est faible. Elle évitera sur ces mêmes tyroliennes d'avoir 
 à tirer sur les bras pour atteindre l'autre rive. Attention toutefois,
 ce roulement à bille particulièrement efficace peut être dangereux. 
 Il n'est à utiliser que sur des parcours déjà connus puisqu'en cas 
 d'angle de descente trop élevé, vous pourriez n'être arrèté que par
 le mur d'en face comme en témoigne de nombreux accidents parfois graves. Il également à noter que la Tandem Speed est interdite sur certine Via Ferrata pour les raisons que nous venons d'indiquer.
Elle est essentiellement utilisée par les professionnels encadrant 
des groupes ou par des amateurs confirmés en complément 
d'une Tandem cable. 
 -------------------------------------            