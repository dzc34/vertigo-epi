
//// Gestion des EPI  /////////////////////////////////////

La gestion des EPI concerne le matos réglementé,
nécessitant une traçabilité et un contrôle régulier.
 
Le matos rentrant dans le cadre 
des EPI sont les suivants :     - casque
                                - baudrier 
                                - mousqueton
                                - corde
                                - sangle
                                - longe                        

En tant qu’association prêtant du matos de type EPI, 
nous avons l'obligation de réaliser un contrôle régulier 
(au moins 1 fois par an) et de sauvegarder les informations 
d'achat (marque, date de fabrication, n° série, ...) 
ainsi que les dates de contrôle et changement (ex : remplacem° des sangles pour les dégaines).

Pour chaque achat, outre l'enregistrement en comptabilité,
il est impératif de conserver la notice du fabricant 
et de sauvegarder les informations du matos de type EPI, à savoir :
    - date d'achat + magasin
    - marque / modèle / n° série ou de lot (si présent)
    - date de fabrication (si présent)
    - date de mise au rebus théorique (à trouver dans la notice)

Idéalement, 1 personne devrait s'occuper de centraliser ces informations


En cas de problème important avec la justice ou les assurances,
une mauvaise gestion des EPI sera un critère aggravant 
dans la responsabilité de l'association.